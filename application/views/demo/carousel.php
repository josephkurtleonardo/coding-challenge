<h1>Javascript &amp; jQuery<h1>
<h2>Requirements</h2>
<p><strong>Task 1: </strong>Using the jQuery library - <a href="http://sorgalla.com/jcarousel/examples/responsive/" target="_blank">http://sorgalla.com/jcarousel/examples/responsive/</a>, create a carousel with the required scripts all embeddded within this view. You may make use of the php loop below to loop through the images so as to populate the images. All right and left buttons should be working</p>

<div id="carousel-home" class="carousel slide" data-ride="carousel" style="width:1000px">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<?php for ($i=0; $i<count($photos); $i++): ?>
		<li data-target="#carousel-home" data-slide-to="<?php echo $i; ?>" class="<?php if ($i==0) echo "active"; ?>"></li>
		<?php endfor; ?>
	</ol>
	
	<!-- Wrapper for slides -->
	<div class="carousel-inner" role="listbox">
		<?php for ($i=0; $i<count($photos); $i++): ?>
		<div class="item <?php if ($i==0) echo "active"; ?>">
			<img src="<?php echo $photos[$i]->image_url; ?>" alt="Photo <?php echo $i+1; ?>" />
			<div class="carousel-caption">
				<p>Image Source: <a href="https://unsplash.com/">https://unsplash.com/</a></p>
			</div>
		</div>
		<?php endfor; ?>
	</div>

	<!-- Controls -->
	<a class="left carousel-control" href="#carousel-home" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="right carousel-control" href="#carousel-home" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>

<!--Start Coding-->

<!--End Coding-->

<br style="clear:both;" />

<h2>Requirements</h2>
<p><strong>Task 2: </strong> </p>
<ol>
<li>Write a XMLHttpRequest in Javascript to get the contact details of a person using the following API call</li>
<li>Method: POST</li>
<li>URL: <a href="http://im.gdbg.com:8080/commsapplib/ws/clGetContactDetails">http://im.gdbg.com:8080/commsapplib/ws/clGetContactDetails</a></li>
<li>Parameters: webId: 'daryll'</li>
<li>Sample result: <code>{"error":null,"data":{"status":"1","Contact":{"recordId":null,"mobileNumber":"96410182","othernumbers":null,"dialingCode":"65","firstName":"Daryll","lastName":"Chu","profileImage":null,"email":"daryll@gdbg.com","otheremails":null,"address":null,"birthday":null,"webId":"daryll","friend":null,"userId":null,"latitude":0.0,"longitude":0.0,"lastUpdated":null,"otherdates":{},"socialProfiles":{},"geoPoint":{"lat":null,"lng":null},"contactAddRequestStatus":null}},"result":"Success"}</code></li>
<li>Display the results of the contact (for eg. recordId, mobileNumber, otherNumbers etc) into a table with <code>th</code> with all the return values within <code>tr</code> and <code>td</code></li>
</ol>

<!--Start Coding-->

<!--End Coding-->